from django.apps import AppConfig


class DolipieappConfig(AppConfig):
    name = 'dolipieapp'
