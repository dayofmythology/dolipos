function chartIt() {

    var progress = document.getElementById('animationProgress');
    labels_data = [];
    invoice_per_month = [];
    month_json = {{ my_context | safe }}; // parse json object from view

    for (x in month_json) {
        labels_data.push(x);
        invoice_per_month.push(month_json[x]);
    }

    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels_data,
            datasets: [{
                label: 'Nombre de Factures par mois',
                fill: true,
                data: invoice_per_month,
                backgroundColor: 'rgba(255, 99, 132, 1)',
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            responsive: false
        }
    });

}