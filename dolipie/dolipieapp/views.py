import requests
from django.shortcuts import render
from datetime import datetime
import json


# Create your views here.
def returnJsonInvoiceResult(data):
    month_dict = {"Jan":0,"Feb":0,"Mar":0,"Apr":0,"May":0,"Jun":0,"Jul":0,"Aug":0,"Sep":0,"Oct":0,"Nov":0,"Dec":0}
    theJSON = json.loads(data)
    for item in theJSON:
        timestamp = item['date']
        month = datetime.fromtimestamp(timestamp).strftime('%b')
        month_dict[f'{month}']+=1
    return month_dict
    
def index(request):
    url='http://localhost/dolibarr/htdocs/api/index.php/invoices'
    r = requests.get(url, headers={"DOLAPIKEY":"xxxxxxxxxxxxxxxxxx"})
    my_context = json.dumps(returnJsonInvoiceResult(r.text))
    return render(request,'dolipieapp/home.html',{'my_context':my_context})